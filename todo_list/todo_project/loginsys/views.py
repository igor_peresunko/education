from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.views.generic.edit import FormView
from django.contrib.auth import login, logout, authenticate
from django.http import HttpResponseRedirect
from django.views.generic.base import View


class RegisterFormView(FormView):
    """Register the user with the subsequent authorization"""

    form_class = UserCreationForm
    success_url = '/todo'
    template_name = "loginsys/register.html"

    def form_valid(self, form):
        form.save()
        username = form.cleaned_data['username']
        password = form.cleaned_data['password1']
        user = authenticate(username=username, password=password)
        login(self.request, user)
        return super(RegisterFormView, self).form_valid(form)


class LoginFormView(FormView):
    """Performs authentication and authorization of the user"""

    form_class = AuthenticationForm
    template_name = "loginsys/login.html"
    success_url = '/todo'

    def form_valid(self, form):
        self.user = form.get_user()
        login(self.request, self.user)
        return super(LoginFormView, self).form_valid(form)


class LogoutView(View):
    def get(self, request):
        logout(request)

        return HttpResponseRedirect("/")
