__author__ = 'Igor Peresunko'

import os
from werkzeug.wrappers import Request, Response
from werkzeug.routing import Map, Rule
from werkzeug.exceptions import HTTPException
from werkzeug.wsgi import SharedDataMiddleware
from jinja2 import Environment, FileSystemLoader
from drawer import Drawer
from file_editor import WorkWithFiles
import time


def correct_sequence_parameters(input_param):
    """
    A dictionary of parameters generates
    the correct order to draw shapes
    """
    params = []
    type = input_param['type']
    params.append(type)
    size = input_param['size']
    params.append(size)
    position = [input_param['x_coord'], input_param['y_coord']]
    params.append(position)
    color = input_param['color']
    params.append(color)
    count_of_angle = input_param['count_of_angle']
    params.append(count_of_angle)
    return params


class Editor():
    def __init__(self):
        self.wsgi_app = SharedDataMiddleware(self.wsgi_app, {
            '/static': os.path.join(os.path.dirname(__file__), 'static')
        })
        template_path = os.path.join(os.path.dirname(__file__), 'templates')
        self.jinja_env = Environment(loader=FileSystemLoader(template_path),
                                     autoescape=True)
        self.url_map = Map([
            Rule('/', endpoint='create'),
            Rule('/create_file', endpoint='create_file'),
            Rule('/draw', endpoint='draw')
        ])

        self.file_name = ''
        self.params = {}  # Stores the name of the file
        self.file = {}  # Stores parameters of the figures
        
    def __call__(self, environ, start_response):
        return self.wsgi_app(environ, start_response)

    def wsgi_app(self, environ, start_response):
        request = Request(environ)
        response = self.dispatch_request(request)
        return response(environ, start_response)

    def dispatch_request(self, request):
        adapter = self.url_map.bind_to_environ(request.environ)
        try:
            endpoint, values = adapter.match()
            return getattr(self, 'on_' + endpoint)(request, **values)
        except HTTPException, e:
            return e

    def render_template(self, template_name, **context):
        t = self.jinja_env.get_template(template_name)
        return Response(t.render(context), mimetype='text/html')

    def on_create(self, request):
        """Shows a shape file creation"""
        response = Response()
        return self.render_template('create_file.html', error=True)

    def on_create_file(self, request, **values):
        """Takes a filename and stores for further processing"""
        wf = WorkWithFiles()  # Object for working with files
        if request.method == 'POST':
            for key in request.form.keys():
                self.file[key] = request.form[key]
        wf.create_file(self.file['file_name'])
        self.file_name = self.file['file_name']
        return self.render_template('receive_settings.html')

    def on_draw(self, request, **values):
        """
        Takes the data from the form, converts in the
        correct order and figure drawing
        """
        draw_obj = Drawer()  # Object to work with the rendering of figures
        file_name = self.file_name
        if request.method == 'POST':
            if os.path.getsize(file_name + '.txt') != 0:
                draw_obj.draw_all(file_name)

            for key in request.form.keys():
                if key != 'type' and key != "color":
                    try:
                        self.params[key] = int(request.form[key])
                    except ValueError:
                        error = "Enter the numbers in all the fields except for the color and type"
                        return self.render_template('receive_settings.html', error=error,
                                                    my_img='http://localhost:8080/static/img/' +
                                                           (file_name + '.svg') + '?r=' + str(time.time()))
                else:
                    self.params[key] = request.form[key]
            if self.params['type'] == 'p':
                if self.params['count_of_angle'] == 0:
                    error = "Due to the zero option in the number of angles is ZeroDivisionError!"
                    return self.render_template('receive_settings.html', error=error,
                                                my_img='http://localhost:8080/static/img/' +
                                                       (file_name + '.svg') + '?r=' + str(time.time()))

        correct_param = correct_sequence_parameters(self.params)
        draw_obj.draw(correct_param, file_name)  # Draw the figure
        draw_obj.save_img(file_name)

        return self.render_template('receive_settings.html',
                                    my_img='http://localhost:8080/static/img/' +
                                           (file_name + '.svg') + '?r=' + str(time.time()))


if __name__ == '__main__':
    from werkzeug.serving import run_simple

    app = Editor()
    run_simple('localhost', 8080, app, use_debugger=True, use_reloader=True)

    # TODO: deleting, editing shapes
    # TODO: validation of input parameters (ok)
    # TODO: to remove the path  (ok)
    # TODO: crash when no file records settings figures (ok)
