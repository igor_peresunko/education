__author__ = 'Igor Peresunko'
import json


class WorkWithFiles:
    """Class for working with files"""

    def __init__(self):
        self.params = []

    def create_file(self, input_name):
        """Create a new file"""
        file_name = input_name + '.txt'
        f = open(file_name, 'w')
        f.write('')
        f.close()

    def read_in(self, input_name):
        """Reading from a file"""
        file_name = input_name + '.txt'
        f = open(file_name)
        for line in f:
            decoded_params = json.loads(line)
            self.params.append(decoded_params)
        return self.params

    def filing(self, params, input_name):
        """Write to the file"""
        file_name = input_name + '.txt'
        f = open(file_name, 'a')
        for line in params:
            f.write(json.dumps(line) + '\n')
        f.close()
        print("save params to the file done")

    def edit_file(self, file_name):  # in process
        """Get params into the file and edit"""
        f = open(file_name)
        for line in f:
            params = json.loads(line)
            self.params.append(params)
        f.close()
        return self.params

    def rewrite_in(self, params, file_name):  # in process
        """Rewrite new params to the file"""
        f = open(file_name, 'w')
        for line in params:
            f.write(json.dumps(line) + '\n')
        f.close()
