__author__ = 'Igor Peresunko'
# -*- coding: utf-8 -*-
import turtle
import os
import canvasvg
from file_editor import *
from figure import *

wf = WorkWithFiles()


class Drawer(WorkWithFiles):
    """Class to draw shapes on the parameters"""

    def __init__(self):
        WorkWithFiles.__init__(self)
        self.wn = turtle.Screen()
        self.figures = {
            'c': Circle,
            'p': Polygon,
            't': Triangle
        }

    def draw_all(self, file_name):
        """Draw all figures into files"""
        self.wn.reset()
        params = wf.read_in(file_name)
        for fig_params in params:
            figure_type = fig_params[0]
            fig = self.figures[figure_type](*fig_params[1:])
            fig.draw()
        params[:] = []  # Clears the list that would not be repeated renderings

    def draw(self, params, input_name):
        """Draw the selected figure by parameters"""
        figure_type = params[0]
        parameters = [params]
        fig = self.figures[figure_type](*params[1:])
        fig.draw()
        wf.filing(parameters, input_name)

    def save_img(self, input_name):
        """Save canvas to svg format"""
        file_name = input_name + '.svg'
        wn = turtle._getscreen().getcanvas()
        old_path = os.getcwd()
        new_path = os.path.join(os.path.dirname(__file__), 'static/img')
        os.chdir(new_path)

        canvasvg.saveall(file_name, wn)
        os.chdir(old_path)
        print('save image done')


if __name__ == "__main__":
    a = Drawer()
    a.save_img('hello')
