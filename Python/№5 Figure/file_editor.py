__author__ = 'Igor Peresunko'
import json


class WorkWithFiles:
    """Class for working with files"""

    def __init__(self):
        self.params = []

    def comparison_types(self, t1, t2):
        """Comparison of two types"""
        if type(t1) == type(t2):
            print(type(t1, t2))
            return True
        else:
            return False

    def create_file(self):
        """Create a new file"""
        file_name = raw_input("Enter the name of file to create:\n")
        f = open(file_name, 'w')
        f.write('')
        f.close()

    def read_in(self):
        """Reading from a file"""
        file_name = raw_input('Enter the name of file:\n')
        f = open(file_name)
        for line in f:
            decoded_params = json.loads(line)
            self.params.append(decoded_params)
        return self.params

    def filing(self, params):
        """Write to the file"""
        file_name = raw_input('Enter the name of the file to maintain figure:\n')
        f = open(file_name, 'a')
        for line in params:
            f.write(json.dumps(line) + '\n')
        f.close()

    def edit_file(self, file_name):
        """Get params into the file and edit"""
        f = open(file_name)
        for line in f:
            params = json.loads(line)
            self.params.append(params)
        f.close()
        return self.params

    def change_settings(self):
        """Take old parameters from file and chang"""
        file_name = raw_input('Enter the name of file:\n')
        self.edit_file(file_name)
        numb_lin = len(self.params)

        for i in range(numb_lin):
            print(self.params[i])

        line = int(raw_input("Enter line: "))
        value = int(raw_input("Enter value: "))
        new_val = raw_input("Enter new value: ")

        old_val = self.params[line - 1][value - 1]
        if self.comparison_types(old_val, new_val) == False: #TODO: incorrect comparison of the types of work
            new_val = int(new_val)

        self.params[line - 1][value - 1] = new_val
        self.rewrite_in(self.params, file_name)

    def rewrite_in(self, params, file_name):
        """Rewrite new params to the file"""
        f = open(file_name, 'w')
        for line in params:
            f.write(json.dumps(line) + '\n')
        f.close()
