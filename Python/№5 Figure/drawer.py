__author__ = 'Igor Peresunko'

import turtle
from file_editor import *
from figure import *

wf = WorkWithFiles()


class Drawer(WorkWithFiles):
    """Class to draw shapes on the parameters"""

    def __init__(self):
        WorkWithFiles.__init__(self)
        self.wn = turtle.Screen()
        self.figures = {
            'c': Circle,
            'p': Polygon,
            't': Triangle
        }

    def draw_all(self):
        """Draw all figures into files"""
        self.wn.reset()
        params = wf.read_in()
        for fig_params in params:
            figure_type = fig_params[0]
            fig = self.figures[figure_type](*fig_params[1:])
            fig.draw()
        params[:] = []  # Clears the list that would not be repeated renderings

    def console_output(self):
        """Enter parameters of the figure"""
        fig_type = raw_input('What type of figure do you want?\n')
        size = int(raw_input('What size do you want?'))

        print('Enter position of the figure')
        coord_x = int(raw_input("Enter x coordinates: "))
        coord_y = int(raw_input("Enter y coordinates: "))

        color = raw_input("Enter color of the figure: ")
        count_of_angle = int(raw_input("Enter count of angle for polygon: "))
        self.draw([fig_type, size, [coord_x, coord_y], color, count_of_angle])

    def draw(self, params):
        """Draw the selected figure by parameters"""
        figure_type = params[0]
        parameters = [params]
        fig = self.figures[figure_type](*params[1:])
        fig.draw()
        wf.filing(parameters)
