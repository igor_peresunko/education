__author__ = 'Igor Peresunko'
import turtle


class Shape():
    """Contains the basic essence"""

    def __init__(self, size, position, fill_color):
        self.size = size
        self.position = position
        self.fill_color = fill_color
        self.turtle = turtle.Turtle()

        self.change_of_position(self.position)

    def change_of_position(self, position):
        """A change of coordinates turtles"""
        self.turtle.penup()
        self.turtle.goto(position[0], position[1])
        self.turtle.pendown()
