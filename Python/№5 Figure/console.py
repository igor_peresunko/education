__author__ = 'Igor Peresunko'

from drawer import Drawer

dw = Drawer()
command = ''

COMMAND = {
    'read': dw.draw_all,
    'draw': dw.console_output,
    'create': dw.create_file,
    'edit': dw.change_settings
}

MENU = {
    'read': '*** Draw figures into the file ***',
    'draw': '*** Drawing figures in the parameters ***',
    'create': '*** Create a new file ***',
    'edit': '*** Edit a file ***'
}


def help():
    """Display a menu item"""
    for k in MENU:
        print k, MENU.get(k)
    print(" ")


def selection(command):
    """Invokes a method on the selected command"""
    return COMMAND.get(command, 'command is not found')


def menu(command):
    """Display a menu item when command selected"""
    return MENU.get(command, 'command is not found')


while command != 'exit':
    print("Enter 'help' to display menu items")
    command = raw_input()
    if command == 'help':
        help()
    else:
        menu_item = menu(command)
        selection(command)()
