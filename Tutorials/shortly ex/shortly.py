# -*- coding: utf-8 -*-
__author__ = 'Igor Peresunko'

import os
import redis
import urlparse
from werkzeug.wrappers import Request, Response
from werkzeug.routing import Map, Rule
from werkzeug.exceptions import HTTPException, NotFound
from werkzeug.wsgi import SharedDataMiddleware
from werkzeug.utils import redirect
from jinja2 import Environment, FileSystemLoader
from io import StringIO

class Editor(object):
    parameters = []

    def __init__(self):
        self.wsgi_app = SharedDataMiddleware(self.wsgi_app, {
            '/static': os.path.join(os.path.dirname(__file__), 'static')
        })

        self.url_map = Map([
            Rule('/', endpoint='receive_settings'),
            # Rule('/enter_params', endpoint='enter_params')
        ])

        template_path = os.path.join(os.path.dirname(__file__), 'templates')
        self.jinja = Environment(
            loader=FileSystemLoader(template_path),
            autoescape=True
        )

    def __call__(self, env, resp):
        rreesspp = self.wsgi_app(env, resp)
        return rreesspp(env, resp)

    def render_template(self, template_name, **context):
        print(template_name, context)
        t = self.jinja.get_template(template_name)
        print(t)
        return Response(t.render(context), mimetype='text/html')

    def wsgi_app(self, env, resp):
        request = Request(env)
        '''
        print('------------------------------')
        print(request)
        response = self.get_response_by_request(env, request)
        print('------------------------------')
        print(response)
        print('------------------------------')
        '''
        return self.render_template('receive_settings.html',type = 'test', error=True)  # пример

    def get_response_by_request(self, env, request):
        adapter = self.url_map.bind_to_environ(request.environ)

        try:
            endpoint, values = adapter.match()
            return getattr(self, 'on_' + endpoint)(request, **values)
        except HTTPException, e:
            return e

    def draw(self, request):
        if request.method == "POST":
            try:
                self.parameters = []
            finally:
                print("error to list")

if __name__ == '__main__':
    from werkzeug.serving import run_simple

    app = Editor()
    run_simple('localhost', 4521, app)
